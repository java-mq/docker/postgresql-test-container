package uz.team7.testContainer.book;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import uz.team7.testContainer.IntegrationTest;

import java.util.Optional;

@IntegrationTest
public class BookServiceIT {

    @Autowired
    private BookService bookService;

    @Autowired
    private BookRepository bookRepository;

    private Book book;

    private static final String DEFAULT_NAME = "Book 1";
    private static final String UPDATED_NAME = "Book 2";

    @BeforeEach
    public void setUp() {
        book = getBook();
    }

    @Test
    @Transactional
    void testSaveBook() {
        int sizeBefore = bookRepository.findAll().size();
        bookService.save(book);

        int sizeAfter = bookRepository.findAll().size();
        assert sizeAfter == sizeBefore + 1;
    }

    @Test
    @Transactional
    void testFindById() {
        bookService.save(book);

        Optional<Book> book = bookService.findById(this.book.getId());

        assert book.isPresent();
        assert book.get().getName().equals(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void testFindAll() {
        bookService.save(book);

        assert bookRepository.findAll().size() == bookService.findAll().size();
    }

    @Test
    @Transactional
    void testDeleteBook() {
        bookService.save(book);
        int sizeBefore = bookRepository.findAll().size();

        bookService.deleteById(book.getId());

        int sizeAfter = bookRepository.findAll().size();
        assert sizeAfter == sizeBefore - 1;
    }

    private Book getBook() {
        return new Book()
                .setName(DEFAULT_NAME);
    }

}
